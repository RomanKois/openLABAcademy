<?php

class StaticClass
{
    public static function resoveStudent($json, $name)
    {
        if ($json == null) {
            $json[$name] = array(
                "name" => $name,
                "counter" => 0,
                "entries" => array()
            );

        }

        if (array_key_exists($name, $json)) {
            $json[$name]['counter']++;
            $json[$name]['entries'][] = self::resolveTime($json, $name, vratCas());

            return $json;

        }

        $json[$name] = array(
            "name" => $name,
            "counter" => 1,
            "entries" => array(vratCas())
        );

        return $json;
    }

    private static function resolveTime($json, $name, $time)
    {
        if (vratCas() > "08:00:00") {
            return $time . " --meskanie";
        } else
            return $time;

    }
}