<?php

class NormalClass{

    public function resoveStudent($json, $name){

        //file dont exist
        if($json == null){
            $json[$name] = array(
                "name"=>$name,
                "counter"=> 0,
                "entries"=>array()
            );

        }

        //founded student
        if(array_key_exists($name, $json)){
            $json[$name]['counter']++;
            $json[$name]['entries'][] = self::resolveTime($json, $name, vratCas());

            return $json;
        }

        // new student
            $json[$name] = array(
                "name"=>$name,
                "counter"=> 1,
                "entries"=>array(vratCas())
            );

        return $json;
    }

    private function resolveTime($json, $name,$time){
        if(vratCas() > "08:00:00") {
            return $time . " --meskanie";
        }else
            return $time;

    }

}